package org.homework.nfe204.from

import org.homework.nfe204.IndexInitializer

class FromFile {
    File file

    void start(IndexInitializer indexInitializer) {
        file.eachLine { line ->
            indexInitializer.inject(line)
        }
        indexInitializer.stop()
    }

    static Builder builder() {
        return new Builder()
    }

    static class Builder {
        private String file

        Builder file(String file) {
            this.file = file
            return this
        }

        FromFile build() {
            File inputFile = new File(file)
            assert inputFile.exists()
            FromFile fromFile = new FromFile()
            fromFile.file = inputFile
            return fromFile
        }
    }
}
