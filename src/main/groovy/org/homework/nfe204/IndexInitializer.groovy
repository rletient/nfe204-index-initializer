package org.homework.nfe204

import org.elasticsearch.action.bulk.BulkRequestBuilder
import org.elasticsearch.action.bulk.BulkResponse
import org.elasticsearch.client.transport.TransportClient
import org.elasticsearch.common.settings.Settings
import org.elasticsearch.common.transport.TransportAddress
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.transport.client.PreBuiltTransportClient
import org.homework.nfe204.from.FromFile

// https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html
// docker run -d --name probtp-search-index -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:6.2.4
class IndexInitializer {


    private String index
    private TransportClient client
    private BulkRequestBuilder bulkRequestBuilder

    private int cpt = 0

    private void init() {
        client = new PreBuiltTransportClient(Settings.builder().put("cluster.name", "docker-cluster").build())
                .addTransportAddress(new TransportAddress(InetAddress.getByName("localhost"), 9300))
    }

    void inject(String line) {
        println "inject > $line"
        if (cpt == 0) {
            bulkRequestBuilder = client.prepareBulk()
        }
        cpt++
        bulkRequestBuilder.add(client.prepareIndex(index, index).setSource(line, XContentType.JSON))
        if (cpt > 99) {
            flush()
        }
    }

    private void flush() {
        println "flush"
        cpt = 0
        BulkResponse bulkResponse = bulkRequestBuilder.get()
        if (bulkResponse.hasFailures()) {
            println bulkResponse
        }
    }

    static IndexInitializer buildForIndex(String index) {
        IndexInitializer indexInitializer = new IndexInitializer()
        indexInitializer.index = index
        indexInitializer.init()
        return indexInitializer
    }

    void stop() {
        if (cpt != 0) {
            flush()
        }
        client.close()
    }

    static void main(String[] args) {
        def cli = new CliBuilder(usage: 'java -jar predictor-1.0-SNAPSHOT-shaded.jar]')
        cli.with {
            h longOpt: 'help', 'Show usage information'
            f longOpt: 'file', required: true, args: 1, argName: 'file', 'Nom du fichier à importer'
            i longOpt: 'index', required: true, args: 1, argName: 'index', "Nom de l'index à mettre à jour"
        }
        def options = cli.parse(args)
        if (!options) {
            return
        }
        if (options.h) {
            cli.usage()
        }



        FromFile.builder().file(options.f).build().start(IndexInitializer.buildForIndex(options.i))
    }
}
